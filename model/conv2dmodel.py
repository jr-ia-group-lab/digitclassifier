import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D


class Conv2dmodel:
    epochs = 12
    batch_size = 128
    num_classes = 10
    img_rows, img_cols = 28, 28
    model_save_path = "./model/saved_models/Conv2DModel.h5"
    (trainData, trainLabels), (testData, testLabels) = (None, None), (None, None)
    (x_train, x_test), (y_train, y_test) = (None, None), (None, None)
    model = None
    score = None

    # split the mnist data into train and test
    def load_mnist(self):
        (self.trainData, self.trainLabels), (self.testData, self.testLabels) = mnist.load_data()

    # reshape and scale the data
    def reshape_data(self):
        self.x_train = self.trainData.reshape(60000, 28, 28, 1)
        self.x_test = self.testData.reshape(10000, 28, 28, 1)
        print('x_train shape:', self.x_train.shape)
        print(self.x_train.shape[0], 'train samples')
        print(self.x_test.shape[0], 'test samples')

    # convert class vectors to binary class matrices
    def convert_labels(self):
        self.y_train = keras.utils.to_categorical(self.trainLabels, self.num_classes)
        self.y_test = keras.utils.to_categorical(self.testLabels, self.num_classes)

    # create the MLP model
    def create_model(self):
        self.model = Sequential()
        self.model.add(Conv2D(32, kernel_size=(3, 3),
                              activation='relu',
                              input_shape=(28, 28, 1)))
        self.model.add(Conv2D(64, (3, 3), activation='relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        self.model.add(Flatten())
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(self.num_classes, activation='softmax'))

    # compile the model
    def compile_model(self):
        self.model.compile(loss=keras.losses.categorical_crossentropy,
                           optimizer=keras.optimizers.Adadelta(),
                           metrics=['accuracy'])

    # fit the model
    def fit_model(self):
        self.model.fit(self.x_train, self.y_train,
                       batch_size=self.batch_size,
                       epochs=self.epochs,
                       verbose=1,
                       validation_data=(self.x_test, self.y_test))

    # evaluate the model
    def evaluate_model(self):
        self.score = self.model.evaluate(self.x_test, self.y_test, verbose=0)

    # print the results
    def print_result(self):
        print('Test loss:', self.score[0])
        print('Test accuracy:', self.score[1])

    # save  model_save_path
    def save_model(self):
        self.model.save(self.model_save_path)

    # generate model
    def generate_model(self):
        self.load_mnist()
        self.reshape_data()
        self.convert_labels()
        self.create_model()
        self.compile_model()
        self.fit_model()
        self.evaluate_model()
        self.print_result()
        self.save_model()
