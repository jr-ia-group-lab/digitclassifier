
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.datasets import mnist
from keras.utils import np_utils

# fix a random seed for reproducibility
np.random.seed(9)


class Mlp:
    # user inputs
    nb_epoch = 25
    batch_size = 64
    num_classes = 10
    train_size = 60000
    test_size = 10000
    v_length = 784
    model_save_path = "./model/saved_models/Mlp.h5"
    (trainData, trainLabels), (testData, testLabels) = (None, None), (None, None)
    model = None
    (mTrainLabels, mTestLabels) = (None, None)
    scores = None

    # split the mnist data into train and test
    def load_mnist(self):
        (self.trainData, self.trainLabels), (self.testData, self.testLabels) = mnist.load_data()

    # reshape and scale the data
    def reshape_data(self):
        self.trainData = self.trainData.reshape(self.train_size, self.v_length)
        self.testData = self.testData.reshape(self.test_size, self.v_length)
        self.trainData = self.trainData.astype("float32")
        self.testData = self.testData.astype("float32")
        self.trainData /= 255
        self.testData /= 255

    # convert class vectors to binary class matrices --> one-hot encoding
    def convert_labels(self):
        self.mTrainLabels = np_utils.to_categorical(self.trainLabels, self.num_classes)
        self.mTestLabels = np_utils.to_categorical(self.testLabels, self.num_classes)

    # create the MLP model
    def create_model(self):
        self.model = Sequential()
        self.model.add(Dense(512, input_shape=(self.v_length,)))
        self.model.add(Activation("relu"))
        self.model.add(Dense(256))
        self.model.add(Activation("relu"))
        self.model.add(Dropout(0.2))
        self.model.add(Dense(self.num_classes))
        self.model.add(Activation("softmax"))

    # compile the model
    def compile_model(self):
        self.model.compile(loss="categorical_crossentropy",
                           optimizer="rmsprop",
                           metrics=["accuracy"])

    # fit the model
    def fit_model(self):
        history = self.model.fit(self.trainData,
                                 self.mTrainLabels,
                                 validation_data=(self.testData, self.mTestLabels),
                                 batch_size=self.batch_size,
                                 nb_epoch=self.nb_epoch,
                                 verbose=2)
        return history.history

    # evaluate the model
    def evaluate_model(self):
        self.scores = self.model.evaluate(self.testData, self.mTestLabels, verbose=0)

    # print the results
    def print_result(self):
        print("[INFO] test score - {}".format(self.scores[0]))
        print("[INFO] test accuracy - {}".format(self.scores[1]))

    # save  model_save_path
    def save_model(self):
        self.model.save(self.model_save_path)

    # generate model
    def generate_model(self):
        self.load_mnist()
        self.reshape_data()
        self.convert_labels()
        self.create_model()
        self.compile_model()
        self.fit_model()
        self.evaluate_model()
        self.print_result()
        self.save_model()
