import keras
from keras.utils import plot_model


class Model:

    def predict_digit(self, model_name, model_path, feature_digit):
        model = keras.models.load_model(model_path)
        feature_digit = getattr(self, 'reshape_data_' + model_name)(feature_digit)
        prediction = model.predict(feature_digit)[0]
        bestclass = ''
        bestconf = -1

        # print("image*******************************************************\n",feature_digit)

        for n in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
            if prediction[n] > bestconf:
                bestclass = str(n)
                bestconf = prediction[n]

        print('I think this digit is a ', bestclass, ' with model ', model_name, ' with', str(bestconf * 100),
              '% confidence.')
        return 'I think this digit is a ' + bestclass + ' with model ' + model_name + ' with ' + str(
            bestconf * 100) + '% confidence.'

    @staticmethod
    def reshape_data_Conv2DModel(feature_digit):
        return feature_digit.reshape(1, 28, 28, 1)

    @staticmethod
    def reshape_data_Mlp(feature_digit):
        feature_digit = feature_digit.reshape(1, 784)
        feature_digit = feature_digit.astype("float32")
        feature_digit /= 255
        return feature_digit
