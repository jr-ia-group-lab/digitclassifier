###### **Digit Classifier based on the MNIST collection**

Implements the classic Digit Classifier with 2 models :<br>
<ul><li>A Conv2D model</li>
<li>A Ann model</li></ul>
It also provide a PainApp Tinker tool to draw your own digit and test the 2 algorithms.

run.py #launch the actionapp used to catch event from the PainApp.

NB : the genericity of the program can be improved.



