import warnings

warnings.filterwarnings("ignore")
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)

import logging

logging.getLogger('tensorflow').disabled = True

import importlib
import os
from tkinter import *

from imageutil import *
from paintapp import *
from predictDigit import *


class ActionApp:
    model_path = "./model/saved_models/"
    paint_app = None
    root = None
    var = None
    selectedModel = None

    # initialize
    def __init__(self):
        # load models and import py files
        models = []
        for f in os.listdir(self.model_path):
            module = '.' + f.split('.')[0].lower()
            classmodel = f.split('.')[0]
            print("load module ", module, " with class ", classmodel)
            my_module = importlib.import_module(module, package='model')
            models.append((classmodel, my_module))
        self.dictModels = dict(models)

        self.root = Tk()
        self.var = StringVar()
        self.var.set('Draw a number')
        self.paint_app = PaintApp(self.root)

        button = Button(self.root,
                        text="QUIT",
                        fg="red",
                        command=self.root.destroy)
        button.pack(side=BOTTOM)
        launch = Button(self.root,
                        text="Launch",
                        command=self.launch_ml)
        launch.pack(side=LEFT)
        erase = Button(self.root,
                       text="Clean",
                       command=self.paint_app.clean)
        erase.pack(side=LEFT)
        learning = Button(self.root,
                          text="Learning",
                          command=self.learning_ml)
        learning.pack(side=LEFT)

        label = Label(self.root,
                      textvariable=self.var,
                      bg="dark green",
                      fg='white',
                      width=100,
                      height=2)
        label.pack()
        self.selectedModel = StringVar()
        i = 0
        for f in self.dictModels:
            i = i + 1
            Radiobutton(self.root, text="Model " + f, variable=self.selectedModel, value=f).pack(anchor=W)
            if i == 1:
                self.selectedModel.set(f)

        self.root.mainloop()

    def launch_ml(self):
        img = self.paint_app.get_image().convert("L")
        img = ImageOps.invert(img)
        img = img.resize((28, 28))
        # img.show()
        my_features_data = get_numpy_from_image(img)
        prediction = Model().predict_digit(self.selectedModel.get(), self.model_path + self.selectedModel.get() + '.h5',
                                           my_features_data)
        print(prediction)
        self.var.set(prediction)

    def learning_ml(self):
        print("Launch learning of model ", self.selectedModel.get(), " in ", self.model_path)
        class_ = getattr(self.dictModels[self.selectedModel.get()], self.selectedModel.get().capitalize())
        instance = class_()
        instance.generate_model()
