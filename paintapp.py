from tkinter import Canvas
from PIL import ImageGrab, ImageOps


# define my class
class PaintApp:
    # Define class variables
    drawing_tool = "pencil"
    left_but = "up"
    drawing_area = 0
    x_pos, y_pos = None, None

    # catch mouse down
    def left_but_down(self, event=None):
        self.left_but = 'down'

    # catch mouse up
    def left_but_up(self, event=None):
        self.left_but = 'up'

        self.x_pos = None
        self.y_pos = None

    # catch mouse move
    def motion(self, event=None):

        if self.drawing_tool == 'pencil':
            self.pencil_draw(event)

    # draw pencil
    def pencil_draw(self, event=None):
        if self.left_but == 'down':
            if self.x_pos is not None and self.y_pos is not None:
                event.widget.create_oval(self.x_pos, self.y_pos, event.x, event.y, width=10, fill='black')
            self.x_pos = event.x
            self.y_pos = event.y

    # initialize
    def __init__(self, root):
        self.drawing_area = Canvas(root, width=280, height=280, bg='white')
        self.drawing_area.pack()
        self.drawing_area.bind('<Motion>', self.motion)
        self.drawing_area.bind('<ButtonPress-1>', self.left_but_down)
        self.drawing_area.bind('<ButtonRelease-1>', self.left_but_up)

    # get PIl image
    def get_image(self, event=None):
        box = (self.drawing_area.winfo_rootx(), self.drawing_area.winfo_rooty(),
               self.drawing_area.winfo_rootx() + self.drawing_area.winfo_width(),
               self.drawing_area.winfo_rooty() + self.drawing_area.winfo_height())
        return ImageGrab.grab(bbox=box)

    # clean drawing area
    def clean(self, event=None):
        self.drawing_area.delete("all")
