import numpy as np
import random
from keras.datasets import mnist
from PIL import Image, ImageOps


# To load images to features and labels
def load_images_to_data(image_file_name):
    im2arr = None
    if ".png" in image_file_name:
        img = Image.open(image_file_name).convert("L")
        img = ImageOps.invert(img)
        # img = img.convert('1')
        img.show()
        print("Image size=", img.size)
        img = np.resize(img, (28, 28, 1))
        im2arr = np.array(img, dtype='float32')
        im2arr = im2arr.reshape(1, 28, 28, 1)
    return im2arr


def get_numpy_from_image(img):
    print("Image size=", img.size)
    img = np.resize(img, (28, 28, 1))
    im2arr = np.array(img, dtype='float32')
    return im2arr


def display_images_from_array(nparray):
    testimage = (np.array(nparray, dtype='float')).reshape(28, 28)
    img = Image.fromarray(np.uint8(testimage * 255), 'L')
    return img.show()


def load_random_mnist_image():
    # the data, split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_test = x_test.reshape(10000, 28, 28, 1)
    mnistrandom = random.randint(0, 10000)
    print('x_test shape:', x_test.shape)
    display_images_from_array(x_test[mnistrandom])
    return x_test[mnistrandom]
